import sys
import os
import unittest


from core import operations
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))


class FirstsTests(unittest.TestCase):

    def test_add_function(self):
        self.assertEqual(operations.add_nums(4, 1), 5)
        self.assertEqual(operations.add_nums(100, 101), 201)

    def test_absolute_truth_and_meaning(self):
        assert True


if __name__ == '__main__':
    unittest.main()

