import prometheus_client as prometheus_client
import os
import requests
from core import operations
from flask import Flask, request, Response
from healthcheck import HealthCheck
from utils.metrics import setup_metrics


CONTENT_TYPE_LATEST = str('text/plain; version=0.0.4; charset=utf-8')

app = Flask(__name__)
setup_metrics(app)
health = HealthCheck(app, "/health")


@app.route("/")
def hello():
    return "Hello, world!"


@app.route("/simple_method")
def simple_method():
    return "This is a simple method"


@app.route("/cool")  # /cool?user=<username>
def cool():
    user = request.args.get('user', default='foo', type=str)
    return '{} {}'.format(user, " is SUPER cool!")


@app.route("/add")  # /add?first_n=<number>&second_n=<number>
def add():
    first_n = request.args.get('first_n', default=1, type=int)
    second_n = request.args.get('second_n', default=1, type=int)

    result = operations.add_nums(first_n, second_n)

    return '{} {}'.format("The result is ", result)


@app.route("/health")
def healthcheck():
    r = requests.get('http://127.0.0.1:5000/')
    if r.status_code == "200":
        return True
    else:
        return False

@app.route('/metrics')
def metrics():
    return Response(prometheus_client.generate_latest(), mimetype=CONTENT_TYPE_LATEST )


if __name__ == "__main__":
    try:
        print(os.environ.get("TOKEN"))
        app.run(host='0.0.0.0', port=80)

    except ValueError:
        print("It has been an error")

